# Automation Nexus3 Repesitory

## Introduction

It is an automated deployment of the Nexus3 repository. Based on Jenkins and Ansible.

By connecting to the control panel, the automation process will execute the playbook on a new VM and create the Nexus3 repository.


## Setup

1. Clone the repo with ```git clone https://gitlab.com/SharahovichDani/ansible_docker_container_project.git``` 

2. Use your Jenkins application and install the SSH Agent Plugin and Credential Binding plugin.

3. You should have two virtual machines, one for the control panel and the other for the Nexus 3.

4. Add to hosts file the public IP of Nexus3 machine
```
[nexus]
>>> Public ip nexus3 <<<
[nexus:vars]
ansible_ssh_private_key_file=/root/ansible/ssh-key.pem
ansible_user=root

```

5. Add your credentials and SSH key to Jenkins.

* In Jenkins, go to Manage Jenkins > Manage Credentials > Global credentials > Add credentials

* Use SSH username with private key.
```
ID: ansible_server_key

Username: Your username that will connect to the VM

Key: copy your ssh key
```

6. Create a new repository in Git and push it to it.

7. Create a pipeline in Jenkins and choose your repository at Source Code Management.

8. When you build your pipeline, it will ask for parameters. Write your public IP address for the control panel.



## Tools Requirements

* Jenkins

* Ansible

* Nexus







